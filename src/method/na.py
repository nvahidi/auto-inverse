import os
import time

import numpy
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset

from .. import inversion, loss, model, oracle, settings, train, utils


def train_forward_model(
            config: dict
        ) -> None:

    forward_model = model.na.create_forward_model(
        layers_size=[config['design_size']] + config['na']['forward_model']['hidden_layers_size'] +
                    [config['performance_size']]
    )

    train_tensor_dataset, test_tensor_dataset = utils.get_train_test_tensor_datasets(
        tensor_dataset=TensorDataset(
            utils.load_tensor_from_mat_file(path=config['design_path']),
            utils.load_tensor_from_mat_file(path=config['performance_path'])
        ),
        train_split_ratio=config['na']['forward_model']['train_split_ratio']
    )
    train_data_loader = DataLoader(
        dataset=train_tensor_dataset,
        batch_size=config['na']['forward_model']['batch_size'],
        shuffle=True,
        pin_memory=True
    )
    test_data_loader = DataLoader(
        dataset=test_tensor_dataset,
        batch_size=config['na']['forward_model']['batch_size'],
        shuffle=True,
        pin_memory=True
    )

    optimizer = optim.Adam(forward_model.parameters(), lr=config['na']['forward_model']['learning_rate'])
    criterion = nn.MSELoss()
    learning_rate_scheduler = optim.lr_scheduler.ExponentialLR(
        optimizer=optimizer,
        gamma=config['na']['forward_model']['learning_rate_decay_rate']
    )

    start_time = time.time()
    loss_value = train.na.train_forward_model(
        forward_model=forward_model,
        criterion=criterion,
        optimizer=optimizer,
        learning_rate_scheduler=learning_rate_scheduler,
        train_data_loader=train_data_loader,
        test_data_loader=test_data_loader,
        number_of_epochs=config['na']['forward_model']['number_of_epochs']
    )
    training_time = time.time() - start_time
    number_of_trainable_parameters = int(sum(p.numel() for p in forward_model.parameters() if p.requires_grad))

    data = {
        "training_time": training_time,
        "number_of_trainable_parameters": number_of_trainable_parameters,
        "loss": loss_value
    }

    root_path = utils.get_na_forward_model_directory(
        results_base_path=config['results_base_path'],
        experiment_name=config['experiment_name'],
        config_name=config['config_name']
    )
    utils.create_directory(directory=root_path)

    utils.save_json_file(data=data, path=os.path.join(root_path, 'info.json'))
    utils.save_model(model=forward_model, path=os.path.join(root_path, 'model.pt'))


def invert_target(
            config: dict,
            experiment_repetition_id: int,
            inversion_repetition_id: int,
            target_name: str
        ) -> None:

    forward_models = [model.na.load_forward_model(config=config)]

    target = utils.load_tensor_from_mat_file(path=config[f'{target_name}_path']).to(settings.DEVICE)
    design = torch.rand(
        target.shape[0], config['design_size']
    ).type(torch.float).to(settings.DEVICE).clone().detach().requires_grad_(True)

    optimizer = optim.Adam([design], lr=config['na']['inversion']['learning_rate'])

    start_time = time.time()
    loss_value, design, forward_mean, no_reduction_loss = inversion.na.converge_design(
        forward_models=forward_models,
        criterion=getattr(loss.na, config['na']['inversion']['criterion']),
        no_reduction_criterion=getattr(loss.na, config['na']['inversion']['no_reduction_criterion']),
        optimizer=optimizer,
        design=design,
        target=target,
        number_of_iterations=config['na']['inversion']['number_of_iterations'],
        minimum_convergence_length=config['na']['inversion']['minimum_convergence_length'],
        convergence_threshold=config['na']['inversion']['convergence_threshold']
    )
    training_time = time.time() - start_time

    data = {
        "inversion_time": training_time,
        "loss": loss_value
    }

    root_path = utils.get_na_inversion_directory(
        results_base_path=config['results_base_path'],
        experiment_name=config['experiment_name'],
        config_name=config['config_name'],
        experiment_repetition_id=experiment_repetition_id,
        inversion_repetition_id=inversion_repetition_id,
        target_name=target_name
    )
    utils.create_directory(directory=root_path)

    utils.save_json_file(data=data, path=os.path.join(root_path, 'info.json'))
    utils.save_tensor_to_mat_file(tensor=design, path=os.path.join(root_path, 'design.mat'))
    utils.save_tensor_to_mat_file(tensor=forward_mean, path=os.path.join(root_path, 'performance.mat'))
    utils.save_tensor_to_mat_file(tensor=no_reduction_loss, path=os.path.join(root_path, 'no_reduction_loss.mat'))


def aggregate_results(
            config: dict,
            experiment_repetition_id: int,
            target_name: str
        ) -> None:

    root_path = utils.get_na_inversion_directory(
        results_base_path=config['results_base_path'],
        experiment_name=config['experiment_name'],
        config_name=config['config_name'],
        experiment_repetition_id=experiment_repetition_id,
        inversion_repetition_id=0,
        target_name=target_name
    )
    root_path = os.path.dirname(root_path)

    repetition_ids = sorted(
        [
            int(directory)
            for directory in os.listdir(root_path)
            if directory.isdigit() and os.path.isdir(os.path.join(root_path, directory))
        ]
    )

    total_no_reduction_losses = []
    for i in range(len(repetition_ids)):
        repetition_id = repetition_ids[i]
        total_no_reduction_losses.append(
            utils.load_mat_file(path=os.path.join(root_path, str(repetition_id), 'no_reduction_loss.mat'))['data']
        )
    total_no_reduction_losses = numpy.array(total_no_reduction_losses).T
    batch_size = total_no_reduction_losses.shape[0]

    best_repetition_id_per_element = []
    best_design = []
    best_no_reduction_loss = []
    best_performance = []
    for element_id in range(batch_size):
        best_repetition_id = numpy.argmin(total_no_reduction_losses[element_id, :])

        best_repetition_id_per_element.append(best_repetition_id)
        best_design.append(
            utils.load_mat_file(
                path=os.path.join(root_path, str(best_repetition_id), 'design.mat')
            )['data'][element_id, :]
        )
        best_no_reduction_loss.append(
            utils.load_mat_file(
                path=os.path.join(root_path, str(best_repetition_id), 'no_reduction_loss.mat')
            )['data'][0, element_id]
        )
        best_performance.append(
            utils.load_mat_file(
                path=os.path.join(root_path, str(best_repetition_id), 'performance.mat')
            )['data'][element_id, :]
        )

    aggregated_results_base_path = os.path.join(root_path, 'aggregated')
    if not os.path.exists(aggregated_results_base_path):
        os.makedirs(aggregated_results_base_path)

    utils.save_mat_file(
        data={'data': numpy.array(best_repetition_id_per_element)},
        path=os.path.join(aggregated_results_base_path, 'repetition_id_per_element.mat'),
    )
    utils.save_mat_file(
        data={'data': numpy.array(best_design)},
        path=os.path.join(aggregated_results_base_path, 'design.mat'),
    )
    utils.save_mat_file(
        data={'data': numpy.array(best_no_reduction_loss)},
        path=os.path.join(aggregated_results_base_path, 'no_reduction_loss.mat'),
    )
    utils.save_mat_file(
        data={'data': numpy.array(best_performance)},
        path=os.path.join(aggregated_results_base_path, 'performance.mat'),
    )


def evaluate_results(
            config: dict,
            experiment_repetition_id: int,
            target_name: str
        ) -> None:

    root_path = utils.get_na_inversion_directory(
        results_base_path=config['results_base_path'],
        experiment_name=config['experiment_name'],
        config_name=config['config_name'],
        experiment_repetition_id=experiment_repetition_id,
        inversion_repetition_id=0,
        target_name=target_name
    )
    root_path = os.path.join(os.path.dirname(root_path), 'aggregated')

    getattr(getattr(oracle, config['oracle']), 'evaluate_results')(
        config=config,
        root_path=root_path,
        target_name=target_name
    )
